#!/usr/bin/ruby

%w[rubygems looksee/shortcuts wirble pp ap irb/completion irb/ext/save-history].each do |gem|
  begin
    require gem
  rescue LoadError => err
    warn "Couldn't load #{gem}: #{err}"
  end
end

IRB.conf[:SAVE_HISTORY] = 1000
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb_history"

IRB.conf[:PROMPT_MODE] = :SIMPLE
IRB.conf[:AUTO_INDENT] = true

# ----------------------------------------
#  Set IRB prompt to include project name and env (Rails)
#  app_name[development]> or app_name[production]>, etc...
# ----------------------------------------
if ENV['RAILS_ENV']
  rails_root = File.basename(Dir.pwd)
  prompt = "#{rails_root}[#{ENV['RAILS_ENV']}]"
  IRB.conf[:PROMPT] ||= {}
  IRB.conf[:PROMPT][:RAILS] = {
    :PROMPT_I => "#{prompt}> ",
    :PROMPT_S => "#{prompt}* ",
    :PROMPT_C => "#{prompt}? ",
    :RETURN => "=> %s\n"
  }
  IRB.conf[:PROMPT_MODE] = :RAILS
end


# ----------------------------------------
#  Alias User[3] for User.find(3) in Rails
# ----------------------------------------
if ENV['RAILS_ENV']
  IRB.conf[:IRB_RC] = Proc.new do
    ActiveRecord::Base.instance_eval { alias :[] :find }
    ActiveRecord::Base.logger = Logger.new(STDOUT)
  end
end


# ---------------------------------------
#  allows you to say 'helper :application' in your
#  Rails app, then you can use helpers by calling
#  helper.helper_name on the console
#  http://errtheblog.com/posts/41-real-console-helpers
# ---------------------------------------
if ENV['RAILS_ENV']
  def Object.method_added(method)
    return super(method) unless method == :helper
    (class<<self;self;end).send(:remove_method, :method_added)

    def helper(*helper_names)
      returning $helper_proxy ||= Object.new do |helper|
        helper_names.each { |h| helper.extend "#{h}_helper".classify.constantize }
      end
    end

    helper.instance_variable_set("@controller", ActionController::Integration::Session.new)

    def helper.method_missing(method, *args, &block)
      @controller.send(method, *args, &block) if @controller && method.to_s =~ /_path$|_url$/
    end

    helper :application rescue nil
  end
end

# ---------------------------------------
#  (Array|Object)#grep
#  from http://github.com/joefiorini/dotfiles
# ---------------------------------------
class Array
  def grep(pattern)
    select {|i| i =~ pattern }
  end
end

# ---------------------------------------
#  Benchmark block run time
# ---------------------------------------
def benchmark
  # From http://blog.evanweaver.com/articles/2006/12/13/benchmark/
  # Call benchmark { } with any block and you get the wallclock runtime
  # as well as a percent change + or - from the last run
  cur = Time.now
  result = yield
  print "#{cur = Time.now - cur} seconds"
  puts " (#{(cur / $last_benchmark * 100).to_i - 100}% change)" rescue puts ""
  $last_benchmark = cur
  result
end

# ---------------------------------------
#  Rot13 cipher
# ---------------------------------------
class String
  def rot13
    tr "A-Za-z", "N-ZA-Mn-za-m"
  end
end

# ---------------------------------------
#  Hirb - Pretty ActiveRecord tables in a
#         mysql console table view. Must
#         load after Wirble
# ---------------------------------------
begin
  require 'hirb'
  Hirb.enable
rescue LoadError => err
  warn "Couldn't load Hirb: #{err}"
end

def clear
  system('clear')
end

class Object
  # list methods which aren't in superclass
  def local_methods(obj = self)
    (obj.methods - obj.class.superclass.instance_methods).sort
  end

  def grep(pattern)
    methods.inject([]) {|all,m| all << m if m =~ pattern }
  end
end

def copy(str)
  IO.popen('pbcopy', 'w') { |f| f << str.to_s }
end

def copy_history
  history = Readline::HISTORY.entries
  index = history.rindex("exit") || -1
  content = history[(index+1)..-2].join("\n")
  puts content
  copy content
end

def paste
  `pbpaste`
end

def load_project_irbrc(path)
  return if (path == ENV["HOME"]) || (path == '/')
  load_project_irbrc(File.dirname path)
  irbrc = File.join(path, '.irbrc')
  load irbrc if File.exists?(irbrc)
end

load_project_irbrc Dir.pwd