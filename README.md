# About the dotfiles
These dotfiles are published under the MIT [license](https://bitbucket.org/codatory/dotfiles/src/master/LICENSE).
[Pull requests](https://bitbucket.org/codatory/dotfiles/pull-requests) and [bug reports](https://bitbucket.org/codatory/dotfiles/issues) are welcome.

# Changelog
## 2012.12.18
* Removed railsrc stuff
* Add project-local irbrc support
* Automatic branch configuration with remotes
* Add RVMRC with compilation optimization (double-check arch for your machine)
* Configure pow to leave workers running for longer time periods
* Add ZSH config
	- Some ZSH functions require git-utils
* Remove sources from gemrc

## 2012.09.25
* Added license
* Added readme
* Added gemrc
* Added gitconfig
* Added irbrc
* Added tmux.config
